//
//  ReceipeDiscriptionController.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//

import Foundation
import UIKit

class ReceipeDiscriptionController: UIViewController {
    
    var receipeTitle:String = ""
    var imagelink:String = ""
    var ingradients:String = ""
    var htmllink:String = ""
    
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipeIngredients: UILabel!
    @IBOutlet weak var recipeWebLink: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        print("\(receipeTitle)")
         recipeImage.kf_setImageWithURL(NSURL(string: imagelink)!,placeholderImage: UIImage(named:"us_img_recipe.png"))
        recipeTitle.text = receipeTitle
        recipeIngredients.text = ingradients
        recipeWebLink.text = htmllink
        
//        let button = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Bordered, target: self, action: #selector(ReceipeDiscriptionController.goBack))
//        self.navigationItem.leftBarButtonItem = button
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func goBack()
//    {        
//        navigationController?.popViewControllerAnimated(true)
//    }
}