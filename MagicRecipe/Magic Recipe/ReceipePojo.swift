//
//  ReceipePojo.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//

import Foundation
import ObjectMapper

// pojo of ReceipePojo 
class ReceipePojo : Mappable{
    
    var title:String?
    var href:String?
    var ingredients:String?
    var thumbnail:String?
    
    init(){
        
    }
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        title  <- map["title"]
        href  <- map["href"]
        ingredients  <- map["ingredients"]
        thumbnail  <- map["thumbnail"]
    }
}
