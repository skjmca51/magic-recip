//
//  RecepeLayoutCell.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//

import UIKit

class RecepeLayoutCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageLayerView: UIView!
    
    @IBOutlet weak var receipeImage: UIImageView!
    
    @IBOutlet weak var receipeItem: UILabel!
    
    @IBOutlet weak var ingradients: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
