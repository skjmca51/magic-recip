//
//  SearchRecipeController.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//


import UIKit
import Alamofire
import ObjectMapper
import Kingfisher

class SearchRecipeController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var stopFetching:Bool = true
    
     var emptyTextView = UIView()
    
    var segmentedTab:Int = 0
    
    var pageCount:Int = 3
    
    var keyboardVisible:Bool = false
    
    var receipeLists = [ReceipePojo]()
    
    var pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        emptyTextView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 20, width: 200, height: 50))
        
        //register xib for tableivew
        let receipeXibRegister = UINib(nibName: "RecepeLayoutCell", bundle: nil)
        tableView.registerNib(receipeXibRegister, forCellReuseIdentifier: "itemcell")
        
        // checking for internet connnection
        let ConnectionStatus = CheckInternetConnection().isConnectedToNetwork()
        if ConnectionStatus {
            fetchRecipeItems("",ingredients: "",pagination: pageCount)
        }else{
            let alert = UIAlertController(title: "Error!", message:"No Internet Connection!.", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true){}
        }
        registerForKeyboardNotifications()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receipeLists.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("itemcell",forIndexPath: indexPath) as! RecepeLayoutCell
        cell.selectionStyle = .None
        cell.receipeItem.text = receipeLists[indexPath.row].title
        cell.receipeImage.kf_setImageWithURL(NSURL(string: receipeLists[indexPath.row].thumbnail!)!,placeholderImage: UIImage(named:"us_img_recipe.png"))
        cell.ingradients.text = receipeLists[indexPath.row].ingredients
        cell.backgroundImageLayerView.layer.cornerRadius = 7
        //        cell.viewbackground.layer.masksToBounds = false
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let indexvalue = sender as! Int
        if (segue.identifier == "recipefromSearch") {
            let aboutSegue:ReceipeDiscriptionController = segue.destinationViewController as! ReceipeDiscriptionController
            aboutSegue.receipeTitle = receipeLists[indexvalue].title!
            aboutSegue.htmllink = receipeLists[indexvalue].href!
            aboutSegue.imagelink = receipeLists[indexvalue].thumbnail!
            aboutSegue.ingradients = receipeLists[indexvalue].ingredients!
        }else{
            print("segue exp else")
        }
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if keyboardVisible {
            searchBar.endEditing(true)
        }else{
            print("You selected cell \(indexPath.row)!")
            dispatch_async(dispatch_get_main_queue(),{
                self.performSegueWithIdentifier("recipefromSearch", sender:  indexPath.row)
            })
        }
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row+3 == receipeLists.count {
            if stopFetching {
                fetchRecipeItems("",ingredients: "",pagination: pageCount)
            }
        }
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(SearchRecipeController.keyboardWasShown(_:)),
            name: UIKeyboardDidShowNotification,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(SearchRecipeController.keyboardWillBeHidden(_:)),
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        keyboardVisible = true
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
         keyboardVisible = false
    }
    
    func emptyText(){
        emptyTextView.hidden = false
        emptyTextView.backgroundColor = UIColor.clearColor()
        emptyTextView.alpha = 0.8
        emptyTextView.layer.cornerRadius = 10
        let textLabel = UILabel(frame: CGRect(x: 52, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.whiteColor()
        textLabel.font = UIFont.boldSystemFontOfSize(12)
        textLabel.text = "No recipe found"
        emptyTextView.addSubview(textLabel)
        view.addSubview(emptyTextView)
    }
    
    func removeEmptyText(){
        emptyTextView.removeFromSuperview()
        emptyTextView.hidden = true
    }


    
    func fetchRecipeItems(recipe:String,ingredients:String,pagination:Int){
        removeEmptyText()
        if pageCount == 3 {
            showActivityIndicator()
        }else {
            tableView.tableFooterView?.hidden = false
            pagingSpinner.startAnimating()
            pagingSpinner.color = UIColor.whiteColor()
            pagingSpinner.hidesWhenStopped = false
            tableView.tableFooterView = pagingSpinner
            tableView.tableFooterView?.willRemoveSubview(pagingSpinner)
        }
        
        Alamofire.request(.POST, Constants.RECEIPE_IP+"i=\(recipe)&q=\(ingredients)&p=\(pagination)")
            .responseData { response in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    do{
                        if let statusCode = response.response?.statusCode{
                            if statusCode == 200{
                                let jsonResult = try NSJSONSerialization.JSONObjectWithData(response.result.value!, options: []) as? NSDictionary
                                let title = jsonResult!.objectForKey("title") as! String
                                let receipeJson = jsonResult!.objectForKey("results")  as! [[String : AnyObject]]
                                print("printing\(title)")
                                if receipeJson.count == 0 {
                                    self.stopFetching = false
                                    if self.pageCount == 3 {
                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                            self.emptyText()
                                        })
                                    }
                                }
                                for blog in receipeJson {
                                    let list = ReceipePojo()
                                    // ----- if title is empty not add to list
                                    if let title = blog["title"] as? String {
                                        let trimmedString = title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                                        if trimmedString.isEmpty{
                                            continue
                                        }else{
                                            list.title = trimmedString
                                        }
                                    }
                                    
                                    if let href = blog["href"] as? String {
                                        list.href = href
                                    }
                                    if let ingredients = blog["ingredients"] as? String {
                                        list.ingredients = ingredients
                                    }
                                    if let thumbnail = blog["thumbnail"] as? String {
                                        list.thumbnail = thumbnail
                                    }
                                    self.receipeLists.append(list)
                                }
                                self.pageCount = self.pageCount + 10
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    if self.pageCount == 13 {
                                        self.hideActivityIndicator()
                                    }else{
                                        self.tableView.tableFooterView?.hidden = true
                                    }
                                    self.tableView.reloadData()
                                })
                                
                                // Object Mapper Not used
                                
                                // var receipeJson = [ReceipePojo]()
                                //receipeLists = Mapper<ReceipePojo>().mapArray(receipeJson)!
                                //print("receipeItemsSize:\(receipeLists.count)")
                            }
                        }
                    }
                    catch{
                        print("Exception")
                    }
                }
        }
    }
    
    func showActivityIndicator() {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicatorView.frame = CGRectMake(0, 0, 14, 14)
        activityIndicatorView.color = UIColor.blackColor()
        activityIndicatorView.startAnimating()
        let titleLabel = UILabel.init()
        titleLabel.text = "Loading"
        titleLabel.font = UIFont.italicSystemFontOfSize(14)
        
        let fittingSize = titleLabel.sizeThatFits(CGSizeMake(200.0, activityIndicatorView.frame.size.height))
        titleLabel.frame = CGRectMake(activityIndicatorView.frame.origin.x + activityIndicatorView.frame.size.width + 8, activityIndicatorView.frame.origin.y, fittingSize.width, fittingSize.height)
        
        let titleView = UIView(frame: CGRectMake(((activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width) / 2), ((activityIndicatorView.frame.size.height) / 2), (activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width), (activityIndicatorView.frame.size.height)))
        titleView.addSubview(activityIndicatorView)
        titleView.addSubview(titleLabel)
        
        self.navigationItem.titleView = titleView
    }
    
    func hideActivityIndicator() {
        self.navigationItem.titleView = nil
    }
    
    
    @IBAction func segmentedChanged(sender: AnyObject) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            segmentedTab = 0
            pageCount = 3
            receipeLists.removeAll()
            tableView.reloadData()
            removeEmptyText()
            searchBar.endEditing(true)
        case 1:
             segmentedTab = 1
             pageCount = 3
             receipeLists.removeAll()
             tableView.reloadData()
             removeEmptyText()
             searchBar.endEditing(true)
        default:
             segmentedTab = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print("serachBarText\(searchText)")
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.endEditing(true)
        print("searchText\(searchBar.text)")
        if !(searchBar.text?.isEmpty)!{
            receipeLists.removeAll()
            pageCount = 3
            tableView.reloadData()
            if segmentedTab == 0 {
                 fetchRecipeItems(searchBar.text!,ingredients: "",pagination: pageCount)
            }else{
                fetchRecipeItems("",ingredients: searchBar.text!,pagination: pageCount)
            }
        }
       
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let backgroundImage = UIImage(named: "\(Constants.backgroundImages[randomInt(0,max: Constants.backgroundImages.count-1)]).jpg")
        let backgroundImage1 = UIImage(color: UIColor.blackColor())
        let imageView1 = UIImageView(image: backgroundImage1)
        imageView1.alpha = 0.5
        imageView1.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        imageView1.maintainAspectRatio()
        let imageView = UIImageView(image: backgroundImage)
        imageView.addSubview(imageView1)
        imageView.maintainAspectRatio()
        self.tableView.backgroundView = imageView
    }
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }


}
