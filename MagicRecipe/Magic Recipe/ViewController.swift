//
//  ViewController.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Kingfisher

class ViewController: UIViewController {
    
    var receipeLists = [ReceipePojo]()
    var pageCount:Int = 3
    var pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    var stopFetching:Bool = true
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // adjust scroll
        self.automaticallyAdjustsScrollViewInsets = false
        
        //register xib for tableivew
        let receipeXibRegister = UINib(nibName: "RecepeLayoutCell", bundle: nil)
        tableView.registerNib(receipeXibRegister, forCellReuseIdentifier: "itemcell")
        
        // checking for internet connnection
        let ConnectionStatus = CheckInternetConnection().isConnectedToNetwork()
        if ConnectionStatus {
            fetchRecipeItems(pageCount)
        }else{
            let alert = UIAlertController(title: "Error!", message:"No Internet Connection!.", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default) { _ in
            }
            alert.addAction(action)
            self.presentViewController(alert, animated: true){}
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receipeLists.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("itemcell",forIndexPath: indexPath) as! RecepeLayoutCell
        cell.selectionStyle = .None
        cell.receipeItem.text = receipeLists[indexPath.row].title
        cell.receipeImage.kf_setImageWithURL(NSURL(string: receipeLists[indexPath.row].thumbnail!)!,placeholderImage: UIImage(named:"us_img_recipe.png"))
        cell.ingradients.text = receipeLists[indexPath.row].ingredients
        cell.backgroundImageLayerView.layer.cornerRadius = 7
//        cell.viewbackground.layer.masksToBounds = false
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        dispatch_async(dispatch_get_main_queue(),{
            self.performSegueWithIdentifier("recipedis", sender:  indexPath.row)
        })
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row+3 == receipeLists.count {
            if stopFetching {
                fetchRecipeItems(pageCount)
            }
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexvalue = sender as! Int
        if (segue.identifier == "recipedis") {
            let aboutSegue:ReceipeDiscriptionController = segue.destinationViewController as! ReceipeDiscriptionController
            aboutSegue.receipeTitle = receipeLists[indexvalue].title!
            aboutSegue.htmllink = receipeLists[indexvalue].href!
            aboutSegue.imagelink = receipeLists[indexvalue].thumbnail!
            aboutSegue.ingradients = receipeLists[indexvalue].ingredients!
        }else{
            print("segue exp else")
        }
    }
    
    
    //------------------ start fetching json from receipe api -----------------------------------
    func fetchRecipeItems(pagination:Int){
        if pageCount == 3 {
            showActivityIndicator()
        }else {
            tableView.tableFooterView?.hidden = false
            pagingSpinner.startAnimating()
            pagingSpinner.color = UIColor.whiteColor()
            pagingSpinner.hidesWhenStopped = false
            tableView.tableFooterView = pagingSpinner
            tableView.tableFooterView?.willRemoveSubview(pagingSpinner)
        }
        
        Alamofire.request(.POST, Constants.RECEIPE_IP+"i=&q=&p=\(pagination)")
            .responseData { response in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    do{
                        if let statusCode = response.response?.statusCode{
                            if statusCode == 200{
                                let jsonResult = try NSJSONSerialization.JSONObjectWithData(response.result.value!, options: []) as? NSDictionary
                                let title = jsonResult!.objectForKey("title") as! String
                                let receipeJson = jsonResult!.objectForKey("results")  as! [[String : AnyObject]]
                                print("\(title)")
                                if receipeJson.count == 0 {
                                    self.stopFetching = false
                                }
                                for blog in receipeJson {
                                    let list = ReceipePojo()
                                    // ----- if title is empty not add to list
                                    if let title = blog["title"] as? String {
                                        let trimmedString = title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                                        if trimmedString.isEmpty{
                                            continue
                                           }else{
                                            list.title = trimmedString
                                        }
                                    }
                                    
                                    if let href = blog["href"] as? String {
                                        list.href = href
                                    }
                                    if let ingredients = blog["ingredients"] as? String {
                                        list.ingredients = ingredients
                                    }
                                    if let thumbnail = blog["thumbnail"] as? String {
                                        list.thumbnail = thumbnail
                                    }
                                    self.receipeLists.append(list)
                                }
                                self.pageCount = self.pageCount + 10
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    if self.pageCount == 13 {
                                        self.hideActivityIndicator()
                                    }else{
                                        self.tableView.tableFooterView?.hidden = true
                                    }
                                    self.tableView.reloadData()
                                })
                                
                                // Object Mapper Not used  
                                
                                // var receipeJson = [ReceipePojo]()
                                //receipeLists = Mapper<ReceipePojo>().mapArray(receipeJson)!
                                //print("receipeItemsSize:\(receipeLists.count)")
                            }
                        }
                    }
                    catch{
                        
                    }
                }
        }
    }
    //------------------ End fetching json from receipe api -----------------------------------

    //adding Background Images
    
    func showActivityIndicator() {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicatorView.frame = CGRectMake(0, 0, 14, 14)
        activityIndicatorView.color = UIColor.blackColor()
        activityIndicatorView.startAnimating()
        
        let titleLabel = UILabel.init()
        titleLabel.text = "Loading"
        titleLabel.font = UIFont.italicSystemFontOfSize(14)
        
        let fittingSize = titleLabel.sizeThatFits(CGSizeMake(200.0, activityIndicatorView.frame.size.height))
        titleLabel.frame = CGRectMake(activityIndicatorView.frame.origin.x + activityIndicatorView.frame.size.width + 8, activityIndicatorView.frame.origin.y, fittingSize.width, fittingSize.height)
        
        let titleView = UIView(frame: CGRectMake(((activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width) / 2), ((activityIndicatorView.frame.size.height) / 2), (activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width), (activityIndicatorView.frame.size.height)))
        titleView.addSubview(activityIndicatorView)
        titleView.addSubview(titleLabel)
        
        self.navigationItem.titleView = titleView
    }
    
    func hideActivityIndicator() {
        self.navigationItem.titleView = nil
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let backgroundImage = UIImage(named: "\(Constants.backgroundImages[randomInt(0,max: Constants.backgroundImages.count-1)]).jpg")
        let backgroundImage1 = UIImage(color: UIColor.blackColor())
        let imageView1 = UIImageView(image: backgroundImage1)
        imageView1.alpha = 0.5
        imageView1.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        imageView1.maintainAspectRatio()
        let imageView = UIImageView(image: backgroundImage)
        imageView.addSubview(imageView1)
        imageView.maintainAspectRatio()
        self.tableView.backgroundView = imageView
    }
    
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }


}

