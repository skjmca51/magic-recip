//
//  Constants.swift
//  Magic Recipe
//
//  Created by sunilprava on 02/07/16.
//  Copyright © 2016 Simpragma. All rights reserved.
//

import Foundation
import UIKit

class Constants{
    // Receipe Ip
    static let RECEIPE_IP = "http://www.recipepuppy.com/api/?"
    static let backgroundImages: [String] = ["receipebackground","receipebackground","receipebackground","receipebackground","receipebackground"]    
}

//------------------------maintain aspect ratio---------------------starts
extension UIImageView{
    
    func maintainAspectRatio(){
        self.contentMode = UIViewContentMode.ScaleAspectFill
    }
    
}
//------------------------maintain aspect ratio---------------------ends

public extension UIImage {
    convenience init(color: UIColor, size: CGSize = CGSizeMake(1, 1)) {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(CGImage: image.CGImage!)
    }
}




